const { expect } = require('chai');

describe('Main', () => {
  before(() => {
    console.log('antes');
  });

  after(() => {
    console.log('depois');
  });

  beforeEach(() => {
    console.log('antes de cada');
  });

  afterEach(() => {
    console.log('depois de cada execucao');
  });

  context('test first', () => {
    it('shold 1 bla bla bla ', () => {
      throw new Error('just an error');
    });

    it('shold 2 bla bla bla ', () => {
      throw new Error('just an error');
    });
  });

  context('test 2', () => {
    it('shold t2 1 bla bla bla ', () => {
      //
    });

    it('shold t2 2 bla bla bla ', () => {
      //
    });
  });
});

describe.only('Main2', () => {
  let arr;

  beforeEach(() => {
    arr = [1, 2, 3];
  });

  it('should have size of 4 when push another value to the array', () => {
    arr.push(4);
    expect(arr).to.have.lengthOf(4);
  });

  it('should have a size of 2 when pop a value from the array', () => {
    arr.pop();
    expect(arr).not.include(3);
  });
});
